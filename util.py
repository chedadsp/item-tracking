"""
Utility functions.
"""


def save_to_excel(workbook, output):
    """Saving workbook to excel file."""
    try:
        workbook.save(filename=output)
    except PermissionError:
        confirmation = input(
            'Writing to excel failed. Make sure that {} is closed. Do you want to try again? (y/N) '.format(output))
        if confirmation in ['y', 'Y', 'yes', 'Yes', 'YES']:
            save_to_excel(workbook, output)
        else:
            print('Exiting without saving report.')
