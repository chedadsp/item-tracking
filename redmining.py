"""
Support for weekly report and item tracking.
"""
import argparse
import json

from item_tracking_excel_generator import ItemTrackingExcelGenerator
from redmine_info import RedmineInfo


def read_configs(config_file_name):
    """
    Reading configuration from config file.
    :param config_file_name: name of the configuration file, default is config.json
    :return: configuration
    """
    if not config_file_name:
        config_file_name = "config.json"  # default configuration

    with open(config_file_name) as file:
        config = json.load(file)

    return config['configurations']


def main():
    """
    The application's main entry point.
    """
    configurations = read_configs(args.config)

    for configuration in configurations:
        redmine_info = RedmineInfo(configuration)

        item_tracking_excel_generator = ItemTrackingExcelGenerator(redmine_info)
        item_tracking_excel_generator.generate()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--config', type=str, help='JSON file with your configuration. default: config.json')
    args = parser.parse_args()

    main()
