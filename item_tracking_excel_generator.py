"""
Support for creating excel from item tracking information.
"""
import re

from openpyxl import Workbook
from openpyxl.styles import Side, Border, Font, Alignment, PatternFill
from openpyxl.utils import get_column_letter

import util
from item_tracking_info import ItemTrackingInfo


class ItemTrackingExcelGenerator:
    """
    Class for creating excel from item tracking information.
    """
    TEAM_COL = 1
    ID_COL = 2
    STATUS_COL = 3
    START_SPRINT_COL = 4
    ITEMS_COL = 5
    OVERALL_ITEM_PROGRESS_COL = 6
    PENDING_EFFORT_COL = 7
    TOTAL_EFFORT_COL = 8
    SPRINT_PROGRESS_COL = 9
    TEAM_COL_LETTER = 'A'
    ITEMS_COL_LETTER = 'E'

    def __init__(self, redmine_info):
        self._redmine_info = redmine_info
        self._issues = []
        self._wb = Workbook()
        self._ws = self._wb.active

    def generate(self):
        """
        Generates the excel file.
        """
        item_tracking_info = ItemTrackingInfo(self._redmine_info)
        self._issues = item_tracking_info.get_issues()

        self._add_header(item_tracking_info.get_sprints())
        current_row = 2

        team_separators = [current_row]
        for team_name in self._redmine_info.get_teams():
            for team in item_tracking_info.get_teams():
                if team_name == team.get_team_name():
                    current_row = self._add_issues_to_wb(team, current_row)
                    team_separators.append(current_row)
        team_separators.pop()

        self._set_column_width()

        for column_cells in self._ws.columns:
            self._set_font_style(column_cells)
            self._set_alignment(column_cells)

        self._set_inner_borders()
        self._set_outer_borders(team_separators)

        util.save_to_excel(self._wb, self._redmine_info.get_output())

    def _add_header(self, sprints):
        header = ['TEAM', 'ID', 'STATUS', 'START SPRINT', 'ITEMS', 'OVERALL ITEM PROGRESS', 'PENDING EFFORT',
                  'TOTAL EFFORT', 'CURRENT SPRINT PROGRESS']
        for sprint in sprints:
            sprint_number = self.extract_sprint_number(sprint.get_sprint_name())
            if sprint_number != '' and sprint_number not in header:
                header.append(sprint_number)

        self._ws.append(header)

    def _add_issues_to_wb(self, team, current_row):
        number_of_issues = 0
        first_row = current_row

        for item in team.get_items():
            self._populate_row(item, current_row)
            current_row += 1
            number_of_issues += 1

        if number_of_issues == 0:
            current_row += 1

        self._merge_cells(first_row, current_row - 1, self.TEAM_COL, team.get_team_name())
        self._merge_cells(first_row, current_row - 1, self.SPRINT_PROGRESS_COL, team.get_current_sprint_progress())
        self._ws.cell(row=first_row, column=self.SPRINT_PROGRESS_COL).style = 'Percent'

        return current_row

    def _populate_row(self, item, current_row):
        try:
            self._ws.cell(row=current_row, column=self.ID_COL).value = int(item.get_external_id())
        except:
            self._ws.cell(row=current_row, column=self.ID_COL).value = item.get_external_id()
        self._ws.cell(row=current_row, column=self.STATUS_COL).value = item.get_status()
        self._ws.cell(row=current_row, column=self.START_SPRINT_COL).value = self.extract_sprint_number(
            item.get_first_sprint().get_sprint_name())
        self._ws.cell(row=current_row, column=self.ITEMS_COL).value = item.get_subject()
        self._ws.cell(row=current_row, column=self.OVERALL_ITEM_PROGRESS_COL).value = item.get_item_progress()
        self._ws.cell(row=current_row, column=self.OVERALL_ITEM_PROGRESS_COL).style = 'Percent'
        self._ws.cell(row=current_row, column=self.PENDING_EFFORT_COL).value = item.get_pending_effort()
        self._ws.cell(row=current_row,
                      column=self.TOTAL_EFFORT_COL).value = item.get_total_spent_time() + item.get_pending_effort()

        for sprint in list(item.get_spent_time().keys()):
            j = 0
            while self._ws.cell(row=1, column=self.SPRINT_PROGRESS_COL + j).value is not None and \
                    str(self._ws.cell(row=1, column=self.SPRINT_PROGRESS_COL + j).value) not in sprint:
                j += 1

            if item.get_spent_time().get(sprint) is not None:
                self._ws.cell(row=current_row, column=self.SPRINT_PROGRESS_COL + j).value = item.get_spent_time().get(
                    sprint)

    @staticmethod
    def extract_sprint_number(sprint_name):
        sprint_number = ''
        search_result = re.search('(?<=Sprint )[0-9]+', sprint_name)
        if search_result is not None:
            sprint_number = int(search_result.group())
        return sprint_number

    def _get_issue_id(self, issue):
        external_id = None
        link = None
        for issue_type in self._redmine_info.get_issue_types():
            identifier = issue_type['id']
            search_by = issue_type['search']
            tracker_link = issue_type['link']
            if identifier in issue.subject:
                external_id = re.search(search_by, issue.subject).group()
                link = tracker_link.format(external_id)

        return external_id, link

    def _merge_cells(self, first_row, last_row, column, value):
        self._ws.merge_cells(
            get_column_letter(column) + str(first_row) + ':' + get_column_letter(column) + str(last_row))
        self._ws.cell(row=first_row, column=column).value = value

    def _set_column_width(self):
        for column_id in range(1, self._ws.max_column + 1):
            column_letter = get_column_letter(column_id)
            if column_letter == self.TEAM_COL_LETTER:
                self._ws.column_dimensions[column_letter].width = 20
            elif column_letter == self.ITEMS_COL_LETTER:
                self._ws.column_dimensions[column_letter].width = 60
            else:
                self._ws.column_dimensions[column_letter].width = 12

    def _set_alignment(self, column_cells):
        if column_cells[0].column_letter != self.ITEMS_COL_LETTER:
            for cell in column_cells:
                cell.alignment = Alignment(horizontal='center', vertical='center')
        else:
            for cell in column_cells:
                cell.alignment = Alignment(vertical='center', wrap_text=True)

        column_cells[0].alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)

    def _set_font_style(self, column_cells):
        for cell in column_cells:
            if cell.row == 1:
                cell.font = Font(name='Arial', size=8, bold=True, color='FFFFFF')
                cell.fill = PatternFill(fill_type='solid', fgColor='0093D0')
            elif cell.column == self.TEAM_COL:
                cell.font = Font(name='Arial', size=8, bold=True)
            elif self._ws.cell(row=cell.row, column=self.STATUS_COL).value == "Closed" and \
                    cell.column != self.SPRINT_PROGRESS_COL:
                cell.font = Font(name='Arial', size=8, strikethrough=True)
            else:
                cell.font = Font(name='Arial', size=8, strikethrough=False)

    def _set_inner_borders(self):
        for column in range(1, self._ws.max_column + 1):
            for row in range(1, self._ws.max_row + 1):
                side = Side(border_style='thin', color='0093D0')
                self._ws.cell(row=row, column=column).border = Border(top=side, bottom=side)

    def _set_outer_borders(self, team_separators):
        thick_side = Side(border_style='thick', color='0093D0')
        thin_side = Side(border_style='thin', color='0093D0')
        for row in range(1, self._ws.max_row + 1):
            self._ws.cell(row=row, column=self.TEAM_COL).border = Border(left=thick_side, top=thin_side,
                                                                         bottom=thin_side)
            self._ws.cell(row=row, column=self._ws.max_column).border = Border(right=thick_side, top=thin_side,
                                                                               bottom=thin_side)
        for column in range(1, self._ws.max_column + 1):
            for row in team_separators:
                if column == 1:
                    self._ws.cell(row=row, column=column).border = Border(top=thick_side, bottom=thin_side,
                                                                          left=thick_side)
                elif column == self._ws.max_column:
                    self._ws.cell(row=row, column=column).border = Border(top=thick_side, bottom=thin_side,
                                                                          right=thick_side)
                else:
                    self._ws.cell(row=row, column=column).border = Border(top=thick_side, bottom=thin_side)
            self._ws.cell(row=1, column=column).border = Border(top=thick_side)
            self._ws.cell(row=self._ws.max_row, column=column).border = Border(bottom=thick_side)

        self._ws.cell(row=1, column=self.TEAM_COL).border = Border(top=thick_side, left=thick_side, bottom=thick_side)
        self._ws.cell(row=self._ws.max_row, column=self.TEAM_COL).border = Border(bottom=thick_side, left=thick_side)
        self._ws.cell(row=1, column=self._ws.max_column).border = Border(top=thick_side, right=thick_side,
                                                                         bottom=thick_side)
        self._ws.cell(row=self._ws.max_row, column=self._ws.max_column).border = Border(bottom=thick_side,
                                                                                        right=thick_side)
