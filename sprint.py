"""
Model for sprint.
"""


class Sprint:
    """
    Model for sprint.
    """
    def __init__(self, sprint_name):
        self._sprint_name = sprint_name
        self._issues = []

    def get_sprint_name(self):
        """
        Getter for sprint name.
        :return: sprint name
        """
        return self._sprint_name

    def get_issues(self):
        """
        Getter for issues.
        :return: list of issues
        """
        return self._issues

    def add_issue_to_sprint(self, issue):
        """
        Adding issue to sprint.
        :param issue: issue
        """
        self._issues.append(issue)
