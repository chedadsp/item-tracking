"""
Support for collecting information item tracking information from csv file.
"""
import re
import warnings
from io import StringIO

import pandas as pd
from tqdm import tqdm

from issue import Issue
from item import Item
from sprint import Sprint
from team import Team


class ItemTrackingInfo:
    """
    Class for collecting information.
    """

    def __init__(self, redmine_info):
        self._issue_table = pd.read_csv(StringIO(redmine_info.get_csv_from_redmine(redmine_info.get_items_url())))
        self._redmine_info = redmine_info
        self._issues = []
        self._items = []
        self._sprints = []
        self._teams = []
        self._filter_items_by_team_and_sprint()
        self._create_items()
        self._items.sort(key=self._sort_items_by_first_sprint)
        self._items.sort(reverse=True, key=self._sort_items_by_last_sprint)
        self._sprints.sort(reverse=True, key=self._sort_sprints)

    def get_issues(self):
        """
        Getter for tracked issues.
        :return: list of issues
        """
        return self._issues

    def get_teams(self):
        """
        Getter for teams.
        :return: teams
        """
        return self._teams

    def get_sprints(self):
        """
        Getter for sprints.
        :return: sprints
        """
        return self._sprints

    def _filter_items_by_team_and_sprint(self):
        to_be_removed = []
        for i in range(0, len(self._issue_table.index)):
            keep_team = False
            keep_sprint = False
            for team in self._redmine_info.get_teams():
                sprint_name = self._issue_table['Sprint'][i] if pd.notna(self._issue_table['Sprint'][i]) else None
                if sprint_name is not None and team in sprint_name:
                    keep_team = True

            for sprint in self._redmine_info.get_sprints():
                sprint_name = self._issue_table['Sprint'][i] if pd.notna(self._issue_table['Sprint'][i]) else None
                if sprint_name is not None and sprint in sprint_name:
                    keep_sprint = True

            if not (keep_team and keep_sprint):
                to_be_removed.append(i)

        self._issue_table.drop(self._issue_table.index[to_be_removed], inplace=True)
        self._issue_table.set_index(pd.Index(range(0, len(self._issue_table.index))), inplace=True)

    def _create_items(self):
        self._collect_items_history()
        for i in tqdm(range(0, len(self._issue_table.index))):
            sprint_name = self._issue_table['Sprint'][i] if pd.notna(self._issue_table['Sprint'][i]) else None
            sprint = self._create_sprint(sprint_name)
            if sprint is None:
                continue

            team = self._create_team(sprint, sprint_name)
            if team.get_team_name() not in self._redmine_info.get_teams():
                continue

            subject = self._issue_table['Subject'][i] if pd.notna(self._issue_table['Subject'][i]) else None
            external_id, link = self._get_issue_external_id_and_link(subject)
            if external_id is None:
                continue

            current_sprint = self._redmine_info.get_current_sprint()
            skip_issue = self._redmine_info.get_sprints()[0] == current_sprint
            for j in range(0, len(self._issue_table.index)):
                if external_id in self._issue_table['Subject'][j] and pd.notna(
                        self._issue_table['Sprint'][j]) and current_sprint in self._issue_table['Sprint'][j]:
                    skip_issue = False
            if skip_issue:
                continue

            status = self._issue_table['Status'][i] if pd.notna(self._issue_table['Status'][i]) else None
            item = self._create_item(external_id, subject, link, sprint, team, status)
            if item is None:
                continue

            redmine_id = self._issue_table['#'][i]
            self._create_issues(item, redmine_id)

    def _collect_items_history(self):
        for i in tqdm(range(0, len(self._issue_table.index))):
            subject = self._issue_table['Subject'][i] if pd.notna(self._issue_table['Subject'][i]) else None
            external_id, _ = self._get_issue_external_id_and_link(subject)
            if external_id is None:
                continue

            add_hashtag = True
            for issue_type in self._redmine_info.get_issue_types():
                if issue_type['id'] in external_id:
                    add_hashtag = False

            if add_hashtag:
                external_id = '%23' + str(external_id)
            redmine_issues = self._redmine_info.get_items_by_external_id(external_id)
            new_table = pd.read_csv(StringIO(self._redmine_info.get_csv_from_redmine(redmine_issues)))
            new_table = self._filter_items_by_redmine_id(new_table)
            if not new_table.empty:
                self._issue_table = self._issue_table.append(new_table, ignore_index=True)

    def _filter_items_by_redmine_id(self, new_table):
        for i in range(0, len(self._issue_table.index)):
            rows = new_table.loc[new_table['#'] == self._issue_table['#'][i]]
            if not rows.empty:
                new_table.drop(rows.index.item(), inplace=True)

        return new_table

    def _create_issues(self, item, parent_id):
        issue_table = pd.DataFrame()
        issue_table = self._collect_all_subtasks(parent_id, issue_table)
        for i in range(0, len(issue_table)):
            subject = issue_table['Subject'][i] if pd.notna(issue_table['Subject'][i]) else None
            sprint_name = issue_table['Sprint'][i] if pd.notna(issue_table['Sprint'][i]) else None
            estimated_time = float(issue_table['Estimated time'][i]) if pd.notna(
                issue_table['Estimated time'][i]) else 0
            spent_time = float(issue_table['Spent time'][i]) if pd.notna(
                issue_table['Spent time'][i]) else 0
            pending_effort = float(issue_table['Pending effort'][i]) if pd.notna(
                issue_table['Pending effort'][i]) else 0

            sprint = self._create_sprint(sprint_name)
            if sprint is None:
                continue

            if sprint not in item.get_sprints():
                item.add_sprint(sprint)

            issue = Issue(sprint, item, subject, item.get_team(), item.get_external_id(), item.get_link(),
                          estimated_time, spent_time, pending_effort)
            self._issues.append(issue)
            item.get_team().add_issue(issue)
            sprint.add_issue_to_sprint(issue)
            item.add_subtask(issue)

            item.add_spent_time_to_sprint(spent_time, sprint)
            if self._redmine_info.get_current_sprint() in sprint.get_sprint_name():
                item.add_pending_effort(pending_effort)

        self._calculate_item_progress()

    def _collect_all_subtasks(self, parent_id, issue_table):
        redmine_issues = self._redmine_info.get_tasks_url_by_parent_id(parent_id)
        new_table = pd.read_csv(StringIO(self._redmine_info.get_csv_from_redmine(redmine_issues)))
        issue_table = issue_table.append(new_table, ignore_index=True)
        for issue_id in new_table['#']:
            issue_table = self._collect_all_subtasks(issue_id, issue_table)

        return issue_table

    def _create_sprint(self, sprint_name):
        if sprint_name is None:
            return None

        sprint = self._get_sprint_by_name(sprint_name)
        if sprint is None:
            sprint = Sprint(sprint_name)
            self._sprints.append(sprint)

        return sprint

    def _get_sprint_by_name(self, sprint_name):
        for sprint in self._sprints:
            if sprint is not None and sprint.get_sprint_name() == sprint_name:
                return sprint

        return None

    def _create_team(self, sprint, sprint_name):
        team_name = self._get_team_name(sprint_name)
        team = self._get_team_by_name(team_name)
        if team is None:
            team = Team(team_name, self._redmine_info.get_current_sprint())
            self._teams.append(team)

        if sprint is not None and sprint not in team.get_sprints():
            team.add_sprint(sprint)

        return team

    @staticmethod
    def _get_team_name(sprint_name):
        if sprint_name is not None and isinstance(sprint_name, str):
            search_result = re.search('(?<=Team ).*', sprint_name)
            if search_result is not None:
                return search_result.group()

        return None

    def _get_team_by_name(self, team_name):
        for team in self._teams:
            if team is not None and team.get_team_name() == team_name:
                return team

        return None

    def _get_issue_external_id_and_link(self, subject):
        external_id = None
        link = None
        for issue_type in self._redmine_info.get_issue_types():
            identifier = issue_type['id']
            search_by = issue_type['search']
            tracker_link = issue_type['link']
            if identifier in subject:
                try:
                    external_id = re.search(search_by, subject).group()
                    link = tracker_link.format(external_id)
                except:
                    warnings.warn(subject)

        return external_id, link

    def _create_item(self, external_id, subject, link, sprint, team, status):
        item = self._get_item_by_external_id(external_id)

        if item is None:
            item = Item(external_id, team, link, status)
            self._items.append(item)
            team.add_item(item)

        if item is None:
            return None

        if sprint not in item.get_sprints():
            item.add_sprint(sprint)

        if item.get_first_sprint() == sprint:
            item.set_subject(subject)

        return item

    def _get_item_by_external_id(self, external_id):
        for item in self._items:
            if item.get_external_id() == external_id:
                return item

        return None

    def _calculate_item_progress(self):
        for item in self._items:
            spent_time = item.get_total_spent_time()
            pending_effort = item.get_pending_effort()
            if spent_time > 0 or pending_effort > 0:
                item.set_item_progress(spent_time / (spent_time + pending_effort))

    @staticmethod
    def _sort_items_by_first_sprint(sprint):
        return sprint.get_first_sprint().get_sprint_name()

    @staticmethod
    def _sort_items_by_last_sprint(sprint):
        return sprint.get_last_sprint().get_sprint_name()

    @staticmethod
    def _sort_sprints(sprint):
        return sprint.get_sprint_name()
