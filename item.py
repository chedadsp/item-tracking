"""
Model for item.
"""
import re


class Item:
    """
    Model for item.
    """

    def __init__(self, external_id, team, link, status):
        self._external_id = external_id
        self._subject = ''
        self._team = team
        self._link = link
        self._status = status
        self._sprints = []
        self._subtasks = []
        self.item_progress = 0
        self._pending_effort = 0
        self._total_spent_time = 0
        self._spent_time = {}

    def get_external_id(self):
        """
        Getter for external id.
        :return: external id
        """
        return self._external_id

    def get_subject(self):
        """
        Getter for subject.
        :return: subject
        """
        return self._subject

    def get_team(self):
        """
        Getter for team.
        :return: team
        """
        return self._team

    def get_link(self):
        """
        Getter for link.
        :return: link
        """
        return self._link

    def get_status(self):
        """
        Getter for status
        :return: status
        """
        return self._status

    def get_sprints(self):
        """
        Getter for sprints.
        :return: list of sprints
        """
        return self._sprints

    def get_item_progress(self):
        """
        Getter for item progress.
        :return: item progress
        """
        return self.item_progress

    def get_pending_effort(self):
        """
        Getter for pending effort.
        :return: pending effort
        """
        return self._pending_effort

    def get_total_spent_time(self):
        """
        Getter for total spent time.
        :return: total spent time
        """
        return self._total_spent_time

    def get_spent_time(self):
        """
        Getter for spent time by sprints.
        :return: dictionary of spent times by sprints
        """
        return self._spent_time

    def set_subject(self, subject):
        """
        Setter for subject.
        :param subject: subject
        """
        self._subject = subject

    def set_item_progress(self, item_progress):
        """
        Setter for item progress
        :param item_progress: item progress
        """
        self.item_progress = item_progress

    def calculate_spent_time_and_total_effort(self, sprint):
        """
        Calculating spent time in current sprint and total effort
        :return: item progress in current sprint and total effort
        """
        spent_time_in_current_sprint = self._spent_time.get(sprint) if self._spent_time.get(sprint) is not None else 0
        total_effort = spent_time_in_current_sprint + self._pending_effort

        return spent_time_in_current_sprint, total_effort

    def add_sprint(self, sprint):
        """
        Add sprint to item.
        :param sprint: sprint
        """
        sprint_number = None
        search_result = re.search('(?<=Sprint )[0-9]+', sprint.get_sprint_name())
        if search_result is not None:
            sprint_number = search_result.group()
        self._sprints.append(sprint)
        self._spent_time[sprint_number] = 0

    def add_subtask(self, issue):
        """
        Add subtask to item.
        :param issue: subtask
        """
        self._subtasks.append(issue)

    def add_spent_time_to_sprint(self, spent_time, sprint):
        """
        Add spent time to sprint.
        :param spent_time: spent time
        :param sprint: sprint
        """
        sprint_number = None
        search_result = re.search('(?<=Sprint )[0-9]+', sprint.get_sprint_name())
        if search_result is not None:
            sprint_number = search_result.group()
        self._spent_time[sprint_number] += spent_time
        self._total_spent_time += spent_time

    def add_pending_effort(self, pending_effort):
        """
        Add pending effort.
        :param pending_effort: pending effort
        """
        self._pending_effort += pending_effort

    def get_last_sprint(self):
        """
        Getter for last sprint.
        :return: last sprint
        """
        self._sprints.sort(reverse=True, key=self._sort_sprints_by_name)
        return self._sprints[0]

    def get_first_sprint(self):
        """
        Getter for first sprint.
        :return: first sprint
        """
        sprints_first_to_last = sorted(self._sprints, key=self._sort_sprints_by_name)
        return sprints_first_to_last[0]

    @staticmethod
    def _sort_sprints_by_name(sprint):
        return sprint.get_sprint_name()
