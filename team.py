"""
Model for team.
"""


class Team:
    """
    Model for team.
    """
    def __init__(self, team_name, current_sprint):
        self._team_name = team_name
        self._current_sprint = current_sprint
        self._current_sprint_progress = 0
        self._sprints = []
        self._issues = []
        self._items = []

    def get_team_name(self):
        """
        Getter for team name.
        :return: team name
        """
        return self._team_name

    def get_current_sprint_progress(self):
        spent_time_in_last_sprint = 0
        total_effort = 0
        for item in self._items:
            spent_time_in_last_sprint += item.calculate_spent_time_and_total_effort(self._current_sprint)[0]
            total_effort += item.calculate_spent_time_and_total_effort(self._current_sprint)[1]

        if total_effort != 0:
            self._current_sprint_progress = spent_time_in_last_sprint / total_effort
        elif spent_time_in_last_sprint == total_effort:
            self._current_sprint_progress = 1
        else:
            self._current_sprint_progress = 0
        return self._current_sprint_progress

    def get_sprints(self):
        """
        Getter for sprints.
        :return: list of sprints
        """
        return self._sprints

    def get_items(self):
        """
        Getter for items.
        :return: list of items
        """
        return self._items

    def add_sprint(self, sprint):
        """
        Adding sprint to team.
        :param sprint: sprint
        """
        self._sprints.append(sprint)
        self._sprints.sort(reverse=True, key=self._sort_sprints_by_name)

    def add_issue(self, issue):
        """
        Adding issue to team.
        :param issue: issue
        """
        self._issues.append(issue)

    def add_item(self, item):
        """
        Adding item to team.
        :param item: item
        """
        self._items.append(item)

    @staticmethod
    def _sort_sprints_by_name(sprint):
        return sprint.get_sprint_name()
