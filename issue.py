"""
Model for issue.
"""


class Issue:
    """
    Model for issue.
    """

    def __init__(self, redmine_id, sprint, parent, subject, team, external_id, link,
                 estimated_time=0.0, spent_time=0.0, pending_effort=0.0):
        self._redmine_id = redmine_id
        self._sprint = sprint
        self._parent = parent
        self._subject = subject
        self._estimated_time = estimated_time
        self._spent_time = spent_time
        self._pending_effort = pending_effort
        self._team = team
        self._external_id = external_id
        self._link = link

    def get_redmine_id(self):
        """
        Getter for Redmine id.
        :return: Redmine id
        """
        return self._redmine_id

    def get_sprint(self):
        """
        Getter for sprint.
        :return: sprint
        """
        return self._sprint

    def get_parent(self):
        """
        Getter for parent.
        :return: parent
        """
        return self._parent

    def get_subject(self):
        """
        Getter for subject.
        :return: subject
        """
        return self._subject

    def get_estimated_time(self):
        """
        Getter for estimated time.
        :return: estimated time
        """
        return self._estimated_time

    def get_spent_time(self):
        """
        Getter for spent time.
        :return: spent time
        """
        return self._spent_time

    def get_pending_effort(self):
        """
        Getter for pending effort.
        :return: pending effort
        """
        return self._pending_effort

    def get_team(self):
        """
        Getter for team.
        :return: team
        """
        return self._team

    def get_external_id(self):
        """
        Getter for external id.
        :return: external id
        """
        return self._external_id

    def get_link(self):
        """
        Getter for link.
        :return: link
        """
        return self._link
