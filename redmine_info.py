"""
Support for communication with Redmine.
"""
from datetime import date
from datetime import datetime

import requests
from dateutil.relativedelta import relativedelta
from redminelib import Redmine


class RedmineInfo:
    """
    Class for Redmine information.
    """
    CHOSEN_FIELD = 'c'
    FILTER = 'f'
    VALUE = 'v'
    OPTION = 'op'
    OPEN_SQUARE_BRACKET = '%5B'
    CLOSED_SQUARE_BRACKET = '%5D'
    EQUALS = '%3D'
    GREATER_THAN = '%3E'
    LESS_THAN = '%3C'
    CONTAINS = '%7E'

    def __init__(self, config):
        self._server = config['server']
        self._username = config['username']
        self._password = config['password']
        self._redmine = Redmine(self._server, username=self._username, password=self._password)
        self._project = self._redmine.project.get(config['project'])
        self._output = config['output']
        self._teams = config['teams']
        self._issue_types = config['issue_types']
        self._current_sprint = self.calculate_current_sprint(config)
        self._sprints = []
        if isinstance(config['sprint'], list):
            self._sprints = config['sprint']
            self._sprints.sort()
            first_sprint = self._sprints[0]
            last_sprint = self._sprints[-1]
            self._start_date = self.calculate_start_day_of_sprint(first_sprint) + relativedelta(weeks=-3)
            self._end_date = self.calculate_start_day_of_sprint(last_sprint) + relativedelta(weeks=+2)
        elif isinstance(config['sprint'], str) and config['sprint'] != '':
            self._sprint = config['sprint']
            self._sprints.append(self._sprint)
            self._start_date = self.calculate_start_day_of_sprint(self._sprint) + relativedelta(weeks=-3)
            self._end_date = self.calculate_start_day_of_sprint(self._sprint) + relativedelta(weeks=+2)
        else:
            self._sprint = self._current_sprint
            self._sprints.append(self._sprint)
            self._start_date = self.calculate_start_day_of_sprint(self._sprint) + relativedelta(weeks=-3)
            self._end_date = date.today()

    def get_server(self):
        """
        Getter for server.
        :return: server
        """
        return self._server

    def get_sprints(self):
        """
        Getter for sprints.
        :return: list of sprints
        """
        return self._sprints

    def get_current_sprint(self):
        """
        Getter for current sprint.
        :return: current sprint
        """
        return self._current_sprint

    def get_teams(self):
        """
        Getter for teams.
        :return: list of team names
        """
        return self._teams

    def get_output(self):
        """
        Getter for output file.
        :return: output
        """
        return self._output

    def get_issue_types(self):
        """
        Getter for issue types.
        :return: issue types
        """
        return self._issue_types

    @staticmethod
    def calculate_start_day_of_sprint(sprint):
        """
        Calculate start day of sprint.
        :return: start day
        """
        return datetime.fromisocalendar(int('20' + sprint[:2]), int(sprint[-2:]), 3) + relativedelta(weeks=-2)

    @staticmethod
    def calculate_current_sprint(config):
        """
        Calculate current sprint by today's date.
        :return: current sprint
        """
        if 'current_sprint' in config and config['current_sprint'] != '':
            return config['current_sprint']

        today_date = date.today()
        year, calendar_week, day = today_date.isocalendar()
        year = str(year)[-2:]
        sprint = year

        if calendar_week % 2 == 1:
            sprint = sprint + '{:02d}'.format(calendar_week + 1)
        elif day > 2:
            sprint = sprint + '{:02d}'.format(calendar_week + 2)
        else:
            sprint = sprint + '{:02d}'.format(calendar_week)

        return sprint

    def get_items_url(self):
        """
        Getter for items url by sprint start and end date from configuration file or all of them if omitted.
        :return: items url
        """
        url_string = self._server
        url_string += '/projects/'
        url_string += self._project.name
        url_string += '/issues.csv?'
        url_string += self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=sprint'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=status'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=subject'
        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=tracker_id'

        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=created_on'

        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '='
        url_string += '&' + 'group_by='

        url_string += '&' + self.OPTION + self.OPEN_SQUARE_BRACKET + 'created_on' + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self.GREATER_THAN + self.LESS_THAN

        url_string += '&' + self.OPTION + self.OPEN_SQUARE_BRACKET + 'tracker_id' + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self.EQUALS

        url_string += '&' + self.VALUE + self.OPEN_SQUARE_BRACKET + 'created_on' + self.CLOSED_SQUARE_BRACKET
        url_string += self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self._start_date.strftime('%Y-%m-%d')

        url_string += '&' + self.VALUE + self.OPEN_SQUARE_BRACKET + 'created_on' + self.CLOSED_SQUARE_BRACKET
        url_string += self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self._end_date.strftime('%Y-%m-%d')

        url_string += '&' + self.VALUE + self.OPEN_SQUARE_BRACKET + 'tracker_id' + self.CLOSED_SQUARE_BRACKET
        url_string += self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '=2'

        return url_string

    def get_items_by_external_id(self, external_id):
        """
        Getter for items url by sprint start and end date from configuration file or all of them if omitted.
        :return: items url
        """
        url_string = self._server
        url_string += '/projects/'
        url_string += self._project.name
        url_string += '/issues.csv?'
        url_string += self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=sprint'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=status'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=subject'
        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=tracker_id'

        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=subject'

        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '='
        url_string += '&' + 'group_by='

        url_string += '&' + self.OPTION + self.OPEN_SQUARE_BRACKET + 'subject' + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self.CONTAINS

        url_string += '&' + self.OPTION + self.OPEN_SQUARE_BRACKET + 'tracker_id' + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self.EQUALS

        url_string += '&' + self.VALUE + self.OPEN_SQUARE_BRACKET + 'subject' + self.CLOSED_SQUARE_BRACKET
        url_string += self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '=' + external_id

        url_string += '&' + self.VALUE + self.OPEN_SQUARE_BRACKET + 'tracker_id' + self.CLOSED_SQUARE_BRACKET
        url_string += self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '=2'

        return url_string

    def get_tasks_url_by_parent_id(self, parent_id):
        """
        Getter for tasks url by parent id.
        :param parent_id: id of parent
        :return: tasks url
        """
        url_string = self._server
        url_string += '/projects/'
        url_string += self._project.name
        url_string += '/issues.csv?'
        url_string += self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=sprint'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=subject'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=estimated_hours'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=spent_hours'
        url_string += '&' + self.CHOSEN_FIELD + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=pending_effort'
        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET
        url_string += '=parent_id'
        url_string += '&' + self.FILTER + self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '='
        url_string += '&' + 'group_by='
        url_string += '&' + self.OPTION + self.OPEN_SQUARE_BRACKET + 'parent_id' + self.CLOSED_SQUARE_BRACKET + '='
        url_string += self.EQUALS
        url_string += '&' + self.VALUE + self.OPEN_SQUARE_BRACKET + 'parent_id' + self.CLOSED_SQUARE_BRACKET
        url_string += self.OPEN_SQUARE_BRACKET + self.CLOSED_SQUARE_BRACKET + '=' + str(parent_id)

        return url_string

    def get_csv_from_redmine(self, url):
        """
        Getter for csv from Redmine.
        :param url: Redmine csv url
        :return: csv in string form
        """
        response = requests.get(url, auth=(self._username, self._password))
        return response.text
