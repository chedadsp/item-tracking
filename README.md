# Item Tracking

## Requirements
To run this script, you will need:

 - `python 3.8` 
 - `pip` (sudo apt install python-pip)
 - `virtualenv` (pip install virtualenv)

## Download and installation
```
python -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

Edit config.json to point to correct Redmine server with the right credentials.
After that you just execute redmining.py.
You can also add multiple configurations to created multiple reports.

python redmining.py
python redmining.py -c my_config.json

